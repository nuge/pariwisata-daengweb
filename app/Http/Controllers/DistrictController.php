<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\District;

class DistrictController extends Controller
{
    public function getDistrict()
    {
        $district = District::where('regency_id', request()->regency_id)->get();
        return response()->json($district);
    }
}
