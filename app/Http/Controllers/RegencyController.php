<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Regency;

class RegencyController extends Controller
{
    public function getRegency()
    {
        $regency = Regency::where('province_id', request()->province_id)->get();
        return response()->json($regency);
    }
}
