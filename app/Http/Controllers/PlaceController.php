<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Category;
use App\Province;
use App\Place;
use File;
use DB;

class PlaceController extends Controller
{
    public function index()
    {
        $places = Place::with(['photos', 'category', 'district', 'district.regency', 'district.regency.province'])->orderBy('created_at', 'DESC');
        if (request()->q != '') {
            $places = $places->where('name', 'LIKE', '%' . request()->q . '%');
        }
        $places = $places->paginate(10);
        return view('place.index', compact('places'));
    }

    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->get();
        $provinces = Province::all();
        return view('place.add', compact('provinces', 'categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'category_id' => 'required|exists:categories,id',
            'district_id' => 'required|exists:districts,id',
            'local_ticket_fee' => 'required|integer',
            'int_ticket_fee' => 'required|integer',
            'description' => 'required|string',
            'image' => 'required'
        ]);

        DB::beginTransaction();
        try {
            $place = Place::create([
                'name' => $request->name,
                'category_id' => $request->category_id,
                'district_id' => $request->district_id,
                'local_ticket_fee' => $request->local_ticket_fee,
                'int_ticket_fee' => $request->int_ticket_fee,
                'description' => $request->description,
                'user_id' => auth()->user()->id
            ]);

            $path = storage_path('app/public/image');
            !File::isDirectory($path) ? File::makeDirectory($path):'';
            foreach ($request->image as $key => $image) {
                $name = time() . Str::random(10) . '.' . $image->getClientOriginalExtension();
                $image->storeAs('public/image', $name);
                $place->photos()->create(['name' => $name, 'order' => $key+1]);
            }

            DB::commit();
            return redirect(route('place.index'))->with(['success' => 'Place has been saved']);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $place = Place::with(['district.regency'])->find($id);
        $categories = Category::orderBy('name', 'ASC')->get();
        $provinces = Province::all();
        return view('place.edit', compact('place', 'categories', 'provinces'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'category_id' => 'required|exists:categories,id',
            'district_id' => 'required|exists:districts,id',
            'local_ticket_fee' => 'required|integer',
            'int_ticket_fee' => 'required|integer',
            'description' => 'required|string'
        ]);

        DB::beginTransaction();
        try {
            $place = Place::find($id);
            $place->update([
                'name' => $request->name,
                'category_id' => $request->category_id,
                'district_id' => $request->district_id,
                'local_ticket_fee' => $request->local_ticket_fee,
                'int_ticket_fee' => $request->int_ticket_fee,
                'description' => $request->description,
            ]);

            $path = storage_path('app/public/image');
            !File::isDirectory($path) ? File::makeDirectory($path):'';
            if ($request->image != '' && count($request->image) > 0) {
                //DELETE OLD IMAGE DATA
                $photos = $place->photos->pluck('name');
                foreach ($photos as $val) {
                    File::exists($path . '/' . $val) ? File::delete($path . '/' . $val):'';
                }
                $place->photos()->delete();

                //INSERT NEW IMAGE
                foreach ($request->image as $key => $image) {
                    $name = time() . Str::random(10) . '.' . $image->getClientOriginalExtension();
                    $image->storeAs('public/image', $name);
                    $place->photos()->create(['name' => $name, 'order' => $key+1]);
                }
            }

            DB::commit();
            return redirect(route('place.index'))->with(['success' => 'Place has been updated']);
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        $place = Place::with(['photos'])->find($id);
        $files = $place->photos->pluck('name');
        foreach ($files as $file) {
            File::delete(storage_path('app/public/image/' . $file));
        }
        $place->photos()->delete();
        $place->delete();
        return redirect(route('place.index'))->with(['success' => 'Place has been deleted']);
    }
}
