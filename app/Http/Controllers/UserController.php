<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ConfirmMail;
use App\Province;
use App\User;
use Mail;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('created_at', 'DESC');
        if (request()->status == 'active') {
            $users = $users->active();
        } elseif (request()->status == 'pending') {
            $users = $users->pending();
        }
        $users = $users->paginate(10);
        return view('user.index', compact('users'));
    }

    public function create()
    {
        $provinces = Province::all();
        return view('user.create', compact('provinces'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
            'phone_number' => 'required|string|max:13',
            'role' => 'required',
            'district_id' => 'required|exists:districts,id'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone_number' => $request->phone_number,
            'role' => $request->role,
            'district_id' => $request->district_id,
            'status' => 1
        ]);
        return redirect(route('user.index'))->with(['success' => 'Add User successfully']);
    }

    public function edit($id)
    {
        $user = User::with(['district', 'district.regency'])->find($id);
        $provinces = Province::all();
        return view('user.edit', compact('user', 'provinces'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|exists:users',
            'password' => 'nullable|string|min:8|confirmed',
            'phone_number' => 'required|string|max:13',
            'role' => 'required',
            'district_id' => 'required|exists:districts,id'
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        if ($request->password != '') {
            $user->password = $request->password;
        }
        $user->phone_number = $request->phone_number;
        $user->role = $request->role;
        $user->district_id = $request->district_id;
        $user->save();
        return redirect(route('user.index'))->with(['success' => 'Update User successfully']);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect(route('user.index'))->with(['success' => 'Delete User successfully']);
    }

    public function formConfirm($id)
    {
        $user = User::with(['district', 'district.regency', 'district.regency.province'])->find($id);
        return view('user.confirm', compact('user'));
    }

    public function confirm(Request $request, $id)
    {
        $user = User::find($id);
        $user->update(['status' => 1]);
        if ($request->send_email == 1) {
            Mail::to($user->email)->send(new ConfirmMail($user));
        }
        return redirect(route('user.index'))->with(['success' => 'User Confirmed successfully']);
    }
}
