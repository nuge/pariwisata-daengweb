<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('created_at', 'DESC')->paginate(10);
        return view('category.index', compact('categories'));
    }

    public function create()
    {
        return view('category.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required|string|max:100']);
        $category = Category::firstOrCreate($request->only(['name']));
        return redirect(route('category.index'))->with(['success' => 'Category: ' . $category->name . ' Has been saved!']);
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required|string|max:100']);
        $category = Category::find($id);
        $category->update([ 'name' => $request->name ]);
        return redirect(route('category.index'))->with(['success' => 'Category: ' . $category->name . ' has been updated!']);
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect(route('category.index'))->with(['success' => 'Category: ' . $category->name . ' has been deleted!']);
    }
}
