@extends('layouts.auth')

@section('title')
    <title>Login</title>
@endsection

@section('content')
    <form class="form-signin" action="{{ route('login') }}" method="POST">
        @csrf
        <img class="mb-4" src="https://daengweb.id/front/dw-theme/images/logo-head.png" alt="" width="250" height="80">
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

        <div class="form-group">
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" placeholder="Email address" required autofocus>
            <p class="text-danger text-left">{{ $errors->first('email') }}</p>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Password" required>
            <p class="text-danger text-left">{{ $errors->first('password') }}</p>
        </div>
        <div class="form-group">
            <div class="checkbox mb-3">
                <label><input type="checkbox" name="remember" value="remember-me"> Remember me</label>
            </div>
        </div>

        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>
@endsection
