@extends('layouts.auth')

@section('title')
    <title>Registration</title>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <form class="form-signin" action="{{ route('register') }}" method="POST">
        @csrf
        <img class="mb-4" src="https://daengweb.id/front/dw-theme/images/logo-head.png" alt="" width="250" height="80">
        <h1 class="h3 mb-3 font-weight-normal">Please Register</h1>

        <div class="form-group">
            <label for="name" class="sr-only">Name</label>
            <input type="text" id="name" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Fullname" required autofocus>
            <p class="text-danger text-left">{{ $errors->first('name') }}</p>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" placeholder="Email address" required>
            <p class="text-danger text-left">{{ $errors->first('email') }}</p>
        </div>
        <div class="form-group">
            <label for="phone_number" class="sr-only">Phone Number</label>
            <input type="text" id="phone_number" name="phone_number" class="form-control {{ $errors->has('phone_number') ? 'is-invalid':'' }}" placeholder="Phone Number" required autofocus>
            <p class="text-danger text-left">{{ $errors->first('phone_number') }}</p>
        </div>
        <div class="form-group">
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" placeholder="Password" required>
            <p class="text-danger text-left">{{ $errors->first('password') }}</p>
        </div>
        <div class="form-group">
            <label class="sr-only">Password Confirmation</label>
            <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid':'' }}" placeholder="Password Confirmation" required>
            <p class="text-danger text-left">{{ $errors->first('password_confirmation') }}</p>
        </div>
        <div class="form-group">
            <label class="sr-only">Role</label>
            <select name="role" class="form-control dw-select2 {{ $errors->has('role') ? 'is-invalid':'' }}" required>
                <option value="">Choose Role</option>
                <option value="2">District Government</option>
                <option value="3">Maintener</option>
            </select>
            <p class="text-danger text-left">{{ $errors->first('role') }}</p>
        </div>
        <div class="form-group">
            <label class="sr-only">Province</label>
            <select name="province_id" id="province_id" class="form-control dw-select2 {{ $errors->has('province_id') ? 'is-invalid':'' }}" required>
                <option value="">Choose Province</option>
                @foreach ($provinces as $province)
                    <option value="{{ $province->id }}">{{ $province->name }}</option>
                @endforeach
            </select>
            <p class="text-danger text-left">{{ $errors->first('province_id') }}</p>
        </div>
        <div class="form-group">
            <label class="sr-only">Regency</label>
            <select name="regency_id" id="regency_id" class="form-control dw-select2 {{ $errors->has('regency_id') ? 'is-invalid':'' }}" required>
                <option value="">Choose Regency</option>
            </select>
            <p class="text-danger text-left">{{ $errors->first('regency_id') }}</p>
        </div>
        <div class="form-group">
            <label class="sr-only">District</label>
            <select name="district_id" id="district_id" class="form-control dw-select2 {{ $errors->has('district_id') ? 'is-invalid':'' }}" required>
                <option value="">Choose District</option>
            </select>
            <p class="text-danger text-left">{{ $errors->first('district_id') }}</p>
        </div>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
    </form>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.dw-select2').select2();

            $('#province_id').on('change', function() {
                $('#regency_id').empty()
                $.ajax({
                    url: '/api/regency?province_id=' + $(this).val(),
                    cache: false,
                    success: function(html){
                        $('#regency_id').append(`
                            <option value="">Choose Regency</option>
                        `)
                        $.each(html, function(index, item) {
                            $('#regency_id').append(`
                                <option value=`+ item.id +`>`+ item.name +`</option>
                            `)
                        })
                    }
                });
            })

            $('#regency_id').on('change', function() {
                $('#district_id').empty()
                $.ajax({
                    url: '/api/district?regency_id=' + $(this).val(),
                    cache: false,
                    success: function(html){
                        $('#district_id').append(`
                            <option value="">Choose District</option>
                        `)
                        $.each(html, function(index, item) {
                            $('#district_id').append(`
                                <option value=`+ item.id +`>`+ item.name +`</option>
                            `)
                        })
                    }
                });
            })
        });
    </script>
@endsection