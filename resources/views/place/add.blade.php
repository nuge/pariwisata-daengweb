@extends('layouts.admin')

@section('title')
    <title>Add New Place</title>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Add New Place</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <form action="{{ route('place.store') }}" method="post" enctype="multipart/form-data">
                    @csrf

                    @if (session('error'))
                        <div class="alert alert-danger">{{ session('error') }}</div>
                    @endif

                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Input Name">
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Category</label>
                        <select name="category_id" class="form-control dw-select2 {{ $errors->has('category_id') ? 'is-invalid':'' }}" required>
                            <option value="">Choose</option>
                            @foreach ($categories as $cat)
                                <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>
                        <p class="text-danger">{{ $errors->first('category_id') }}</p>
                    </div>
                    <div class="form-group">
                        <label>Province</label>
                        <select name="province_id" id="province_id" class="form-control dw-select2 {{ $errors->has('province_id') ? 'is-invalid':'' }}" required>
                            <option value="">Choose Province</option>
                            @foreach ($provinces as $province)
                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                            @endforeach
                        </select>
                        <p class="text-danger">{{ $errors->first('province_id') }}</p>
                    </div>
                    <div class="form-group">
                        <label>Regency</label>
                        <select name="regency_id" id="regency_id" class="form-control dw-select2 {{ $errors->has('regency_id') ? 'is-invalid':'' }}" required>
                            <option value="">Choose Regency</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('regency_id') }}</p>
                    </div>
                    <div class="form-group">
                        <label>District</label>
                        <select name="district_id" id="district_id" class="form-control dw-select2 {{ $errors->has('district_id') ? 'is-invalid':'' }}" required>
                            <option value="">Choose District</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('district_id') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Local Fee</label>
                        <input type="number" name="local_ticket_fee" class="form-control {{ $errors->has('local_ticket_fee') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('local_ticket_fee') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">International Fee</label>
                        <input type="number" name="int_ticket_fee" class="form-control {{ $errors->has('int_ticket_fee') ? 'is-invalid':'' }}">
                        <p class="text-danger">{{ $errors->first('int_ticket_fee') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea name="description" id="description" class="form-control" cols="30" rows="10"></textarea>
                        <p class="text-danger">{{ $errors->first('description') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Image</label>
                        <input type="file" class="form-control {{ $errors->has('image') ? 'is-invalid':'' }}" name="image[]" required multiple="multiple">
                        <p class="text-danger">{{ $errors->first('image') }}</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-sm">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
    <script>
        $('.dw-select2').select2();
        CKEDITOR.replace('description');

        $('#province_id').on('change', function() {
            $('#regency_id').empty()
            $.ajax({
                url: '/api/regency?province_id=' + $(this).val(),
                cache: false,
                success: function(html){
                    $('#regency_id').append(`
                        <option value="">Choose Regency</option>
                    `)
                    $.each(html, function(index, item) {
                        $('#regency_id').append(`
                            <option value=`+ item.id +`>`+ item.name +`</option>
                        `)
                    })
                }
            });
        })

        $('#regency_id').on('change', function() {
            $('#district_id').empty()
            $.ajax({
                url: '/api/district?regency_id=' + $(this).val(),
                cache: false,
                success: function(html){
                    $('#district_id').append(`
                        <option value="">Choose District</option>
                    `)
                    $.each(html, function(index, item) {
                        $('#district_id').append(`
                            <option value=`+ item.id +`>`+ item.name +`</option>
                        `)
                    })
                }
            });
        })
    </script>
@endsection