@extends('layouts.admin')

@section('title')
    <title>Management Place</title>
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Management Place</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('place.create') }}" class="btn btn-primary btn-sm">Add New</a>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <form action="{{ route('place.index') }}" method="get">
                            <div class="form-group">
                                <input type="text" placeholder="search" name="q" class="form-control">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-12">
                        @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif
                    </div>
                </div>
                
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Category</th>
                                <th>Location</th>
                                <th>Local Fee</th>
                                <th>Int Fee</th>
                                <th>Created at</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @forelse($places as $key => $row)
                        <tr>
                            <td>
                                <img src="{{ asset('storage/image/' . $row->photos[0]->name) }}" alt="{{ $row->name }}" width="100px" height="80px">
                            </td>
                            <td><strong>{{ $row->name }}</strong></td>
                            <td>{{ $row->category->name }}</td>
                            <td>{{ $row->district->name }}, {{ $row->district->regency->name }}, {{ $row->district->regency->province->name }}</td>
                            <td>Rp {{ number_format($row->local_ticket_fee) }}</td>
                            <td>Rp {{ number_format($row->int_ticket_fee) }}</td>
                            <td>{{ $row->created_at }}</td>
                            <td>
                                <form action="{{ route('place.destroy', $row->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <a href="{{ route('place.edit', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                    <button class="btn btn-danger btn-sm confirm-delete">Remove</button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="text-center" colspan="8">No Data</td>
                        </tr>
                        @endforelse
                    </table>
                </div>
                <div class="float-right">
                    {!! $places->links() !!}
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.confirm-delete').on('click', function(event) {
                event.preventDefault();
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover it!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $(this).closest('form').submit();
                    }
                });
            })
        })
    </script>
@endsection