<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Email Confirmation</title>
</head>
<body>
    <h3>Hi {{ $name }}</h3>
    <p>Your account has been verified, please login in this <a href="{{ route('login') }}">page</a></p>
</body>
</html>