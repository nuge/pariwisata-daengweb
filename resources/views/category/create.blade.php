@extends('layouts.admin')

@section('title')
    <title>Add New Category</title>
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Add New Category</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <form action="{{ route('category.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" placeholder="Input Category Name">
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-sm">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection
