@extends('layouts.admin')

@section('title')
    <title>Add New User</title>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Add New User</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <form action="{{ route('user.store') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('name') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('email') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Phone Number</label>
                        <input type="text" name="phone_number" class="form-control {{ $errors->has('phone_number') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('phone_number') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('password') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Password Confirmation</label>
                        <input type="password" name="password_confirmation" class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid':'' }}" required>
                        <p class="text-danger">{{ $errors->first('password_confirmation') }}</p>
                    </div>
                    <div class="form-group">
                        <label for="">Role</label>
                        <select name="role" class="form-control dw-select2 {{ $errors->has('role') ? 'is-invalid':'' }}" required>
                            <option value="">Choose Role</option>
                            <option value="2">District Government</option>
                            <option value="3">Maintener</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('role') }}</p>
                    </div>
                    <div class="form-group">
                        <label>Province</label>
                        <select name="province_id" id="province_id" class="form-control dw-select2 {{ $errors->has('province_id') ? 'is-invalid':'' }}" required>
                            <option value="">Choose Province</option>
                            @foreach ($provinces as $province)
                                <option value="{{ $province->id }}">{{ $province->name }}</option>
                            @endforeach
                        </select>
                        <p class="text-danger">{{ $errors->first('province_id') }}</p>
                    </div>
                    <div class="form-group">
                        <label>Regency</label>
                        <select name="regency_id" id="regency_id" class="form-control dw-select2 {{ $errors->has('regency_id') ? 'is-invalid':'' }}" required>
                            <option value="">Choose Regency</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('regency_id') }}</p>
                    </div>
                    <div class="form-group">
                        <label>District</label>
                        <select name="district_id" id="district_id" class="form-control dw-select2 {{ $errors->has('district_id') ? 'is-invalid':'' }}" required>
                            <option value="">Choose District</option>
                        </select>
                        <p class="text-danger">{{ $errors->first('district_id') }}</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-sm">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.dw-select2').select2();

            $('#province_id').on('change', function() {
                $('#regency_id').empty()
                $.ajax({
                    url: '/api/regency?province_id=' + $(this).val(),
                    cache: false,
                    success: function(html){
                        $('#regency_id').append(`
                            <option value="">Choose Regency</option>
                        `)
                        $.each(html, function(index, item) {
                            $('#regency_id').append(`
                                <option value=`+ item.id +`>`+ item.name +`</option>
                            `)
                        })
                    }
                });
            })

            $('#regency_id').on('change', function() {
                $('#district_id').empty()
                $.ajax({
                    url: '/api/district?regency_id=' + $(this).val(),
                    cache: false,
                    success: function(html){
                        $('#district_id').append(`
                            <option value="">Choose District</option>
                        `)
                        $.each(html, function(index, item) {
                            $('#district_id').append(`
                                <option value=`+ item.id +`>`+ item.name +`</option>
                            `)
                        })
                    }
                });
            })
        });
    </script>
@endsection