@extends('layouts.admin')

@section('title')
    <title>Management User</title>
@endsection

@section('css')
    <style>
        a.disabled {
            pointer-events: none;
            cursor: default;
        }
    </style>
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Management User</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                <a href="{{ route('user.create') }}" class="btn btn-primary btn-sm">Add New</a>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @endif

                <div class="col-md-4">
                    <form action="{{ route('user.index') }}" method="get">
                        <div class="form-group">
                            <label for="">Filter Status</label>
                            <select name="status" class="form-control">
                                <option value="">Choose Status</option>
                                <option value="pending" {{ request()->status == 'pending' ? 'selected':'' }}>Pending</option>
                                <option value="active" {{ request()->status == 'active  ' ? 'selected':'' }}>Active</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-sm">Filter</button>
                        </div>
                    </form>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th>Created at</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        @forelse($users as $key => $row)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $row->name }}</td>
                            <td>{{ $row->email }}</td>
                            <td>{{ $row->role }}</td>
                            <td>
                                @if ($row->status == 1)
                                <span class="badge badge-success">Active</span>
                                @else
                                <span class="badge badge-secondary">Pending</span>
                                @endif
                            </td>
                            <td>{{ $row->created_at }}</td>
                            <td>
                                <form action="{{ route('user.destroy', $row->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                <a href="{{ route('user.confirm', $row->id) }}" class="btn btn-info btn-sm {{ $row->status == 1 ? 'disabled':'' }}">Confirm</a>
                                    <a href="{{ route('user.edit', $row->id) }}" class="btn btn-warning btn-sm">Edit</a>
                                    <button class="btn btn-danger btn-sm confirm-delete">Remove</button>
                                </form>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td class="text-center" colspan="7">No Data</td>
                        </tr>
                        @endforelse
                    </table>
                </div>
                <div class="float-right">
                    {!! $users->links() !!}
                </div>
            </div>
        </div>
    </main>
@endsection

@section('js')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        $(document).ready(function() {
            $('.confirm-delete').on('click', function(event) {
                event.preventDefault();
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover it!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $(this).closest('form').submit();
                    }
                });
            })
        })
    </script>
@endsection