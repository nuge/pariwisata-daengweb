@extends('layouts.admin')

@section('title')
    <title>Confirm User</title>
@endsection

@section('css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Confirm User</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                                <th>Role</th>
                                <th>Location</th>
                                <th>Registered At</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone_number }}</td>
                                <td>{{ $user->role }}</td>
                                <td>{{ $user->district->name }} - {{ $user->district->regency->name }} - {{ $user->district->regency->province->name }}</td>
                                <td>{{ $user->created_at }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    <form action="{{ route('user.setConfirm', $user->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <input type="checkbox" name="send_email" value="1"> Send Notification?
                        </div>
                        <div class="form-grou">
                            <button class="btn btn-primary btn-sm">Confirm</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
@endsection
