<?php

use Illuminate\Database\Seeder;
use App\Regency;

class RegencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regency_json = \Storage::disk('local')->get('regencies.json');
        $regencies = json_decode($regency_json, true)[2]['data'];
        foreach ($regencies as $row) {
            Regency::firstOrCreate([
                'id' => $row['id'],
                'province_id' => $row['province_id'],
                'name' => $row['name']
            ]);
        }
    }
}
