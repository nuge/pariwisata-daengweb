<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'email' => 'admin@daengweb.id'
        ], [
            'name' => 'Anugrah Sandi',
            'password' => bcrypt('secret'),
            'email_verified_at' => Carbon::now(),
            'role' => 1,
            'status' => 1
        ]);
    }
}
