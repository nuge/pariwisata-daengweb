<?php

use Illuminate\Database\Seeder;
use App\District;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $district_json = \Storage::disk('local')->get('districts.json');
        $districts = json_decode($district_json, true)[2]['data'];
        foreach ($districts as $row) {
            District::firstOrCreate([
                'id' => $row['id'],
                'regency_id' => $row['regency_id'],
                'name' => $row['name']
            ]);
        }
    }
}
