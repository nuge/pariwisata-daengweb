<?php

use Illuminate\Database\Seeder;
use App\Province;

class ProvinceTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $provinces_json = \Storage::disk('local')->get('provinces.json');
        $provinces = json_decode($provinces_json, true)[2]['data'];
        foreach ($provinces as $row) {
            Province::firstOrCreate([
                'id' => $row['id'],
                'name' => $row['name']
            ]);
        }
    }
}
