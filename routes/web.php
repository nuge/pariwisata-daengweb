<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('category', 'CategoryController');

Route::resource('user', 'UserController');
Route::get('/user/{id}/confirm', 'UserController@formConfirm')->name('user.confirm');
Route::put('/user/{id}/confirm', 'UserController@confirm')->name('user.setConfirm');

Route::resource('place', 'PlaceController');